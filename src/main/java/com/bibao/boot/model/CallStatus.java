package com.bibao.boot.model;

public enum CallStatus {
	ACCEPTED, SKIPPED
}
