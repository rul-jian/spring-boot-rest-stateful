package com.bibao.boot.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bibao.boot.entity.CallLimitEntity;

public interface CallLimitRepository extends JpaRepository<CallLimitEntity, String> {

}
