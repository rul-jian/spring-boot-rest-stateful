package com.bibao.boot.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bibao.boot.model.CallInfo;
import com.bibao.boot.service.CallService;

@RestController
@RequestMapping("/call")
public class CallRestService {
	@Autowired
	private CallService service;
	
	@PostMapping("/stateful")
	public CallInfo handleCallStateful(@RequestBody CallInfo info) {
		return service.handleCallsStateful(info);
	}
	
	@PostMapping("/stateless")
	public CallInfo handleCallStateless(@RequestBody CallInfo info) {
		return service.handleCallsStateless(info);
	}
}
