package com.bibao.boot.service;

import com.bibao.boot.model.CallInfo;

public interface CallService {
	public CallInfo handleCallsStateful(CallInfo info);
	
	public CallInfo handleCallsStateless(CallInfo info);
}
