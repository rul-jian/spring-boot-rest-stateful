package com.bibao.boot.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bibao.boot.entity.CallLimitEntity;
import com.bibao.boot.model.Call;
import com.bibao.boot.model.CallBalance;
import com.bibao.boot.model.CallInfo;
import com.bibao.boot.model.CallStatus;
import com.bibao.boot.repository.CallLimitRepository;

@Service
public class CallServiceImpl implements CallService {
	@Autowired
	private CallLimitRepository limitRepo;
	
	private CallBalance callBalance;
	
	@Override
	public CallInfo handleCallsStateful(CallInfo info) {
		if (callBalance==null) {
			callBalance = mapToCallBalance(limitRepo.findAll());
		}
		handleCalls(info, callBalance);
		return info;
	}

	@Override
	public CallInfo handleCallsStateless(CallInfo info) {
		CallBalance balance = mapToCallBalance(limitRepo.findAll());
		handleCalls(info, balance);
		return info;
	}

	private void handleCalls(CallInfo info, CallBalance balance) {
		for (Call call: info.getCalls()) {
			if (handleCall(call, balance)) {
				info.increaseNumOfAccepted();
			} else {
				info.increaseNumOfSkipped();
			}
		}
	}
	
	private boolean handleCall(Call call, CallBalance balance) {
		if (balance.getGroupBalance()<=0) {
			call.setStatus(CallStatus.SKIPPED);
			call.setReason("Call skipped since it exceeds the group limit");
			return false;
		}
		int representativeBalance = balance.getRepresentativeBalance().get(call.getRepresentative());
		if (representativeBalance<=0) {
			call.setStatus(CallStatus.SKIPPED);
			call.setReason("Call skipped since it exeeds " + call.getRepresentative() + "'s limit");
			return false;
		}
		call.setStatus(CallStatus.ACCEPTED);
		balance.reduceGroupBalance();
		balance.getRepresentativeBalance().put(call.getRepresentative(), representativeBalance - 1);
		return true;
	}
	
	private CallBalance mapToCallBalance(List<CallLimitEntity> entities) {
		CallBalance balance = new CallBalance();
		balance.setRepresentativeBalance(new HashMap<>());
		entities.forEach(e -> {
			if (e.getGroupIndicator().equals("Y")) {
				balance.setGroupBalance(e.getLimit());
			} else {
				balance.getRepresentativeBalance().put(e.getId(), e.getLimit());
			}
		});
		return balance;
	}
}
