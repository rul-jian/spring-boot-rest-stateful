package com.bibao.boot.rest;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.bibao.boot.model.Call;
import com.bibao.boot.model.CallInfo;
import com.bibao.boot.springbootreststateful.SpringBootRestStatefulApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootRestStatefulApplication.class)
public class CallRestServiceIntegrationTest {
	@Autowired
	private CallRestService service;
	
	@Test
	public void testHandleCallStateful() {
		CallInfo info = generatePayload();
		CallInfo resultInfo = service.handleCallStateful(info);
		assertEquals(4, resultInfo.getNumOfAccepted());
		assertEquals(2, resultInfo.getNumOfSkipped());
	}

	@Test
	public void testHandleCallStateless() {
		CallInfo info = generatePayload();
		CallInfo resultInfo = service.handleCallStateless(info);
		assertEquals(4, resultInfo.getNumOfAccepted());
		assertEquals(2, resultInfo.getNumOfSkipped());
	}
	
	private CallInfo generatePayload() {
		CallInfo info = new CallInfo();
		info.setCalls(new ArrayList<>());
		info.getCalls().add(new Call(1, "A1"));
		info.getCalls().add(new Call(2, "A1"));
		info.getCalls().add(new Call(3, "A1"));
		info.getCalls().add(new Call(4, "A2"));
		info.getCalls().add(new Call(5, "A2"));
		info.getCalls().add(new Call(6, "A2"));
		return info;
	}
}
